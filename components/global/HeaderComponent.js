import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default class HeaderComponent extends Component {
    render() {
        return (
            <Text style={styles.header}>{this.props.children}</Text>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        fontSize: 36,
        textAlign: 'center',
        marginTop: 47
    }
})
