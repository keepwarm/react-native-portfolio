import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import HeaderComponent from '../components/global/HeaderComponent'

export default class WelcomePage extends Component {
    render() {
        return (
            <View>
                <HeaderComponent>Welcome To My Portfolio</HeaderComponent>
                <Text>My name is Hien Ho</Text>
                <Text>My focus is on Front-end Development</Text>
                <Text>My goals are Full-stack Development and UX Design</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})
