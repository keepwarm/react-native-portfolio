import React, { Component, createRef } from 'react'
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import HeaderComponent from '../components/global/HeaderComponent';
import {Dimensions} from 'react-native';

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

export default class MainComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            activeIndex: 0,
            carouselItems: [
                {
                    title: 'Welcome to Hien Ho Portfolio',
                    image: require('../assets/images/image1.png')
                },
                {
                    title: 'About Hien Ho',
                    image: require('../assets/images/image2.png')
                },
                {
                    title: 'Skills of Hien Ho',
                    image: require('../assets/images/image3.jpg')
                },
                {
                    title: 'Timeline about education and work experiences of Hien Ho',
                    image: require('../assets/images/image4.png')
                }
            ]
        }
    }

    carousel = createRef();

    _renderItem = ({item, index}) => {
        // change StyleSheet.slide to styles.slide
        // import Image
        return(
            <View style={styles.slide}>
                <Image source={item.image} style={styles.image} />
                <Text style={styles.title}>{item.title}</Text>
            </View>
        )
    };

    render() {
        sliderWidth = viewportWidth;
        slideWidth = (viewportWidth *75)/100;
        itemWidth = slideWidth + ((viewportWidth * 2)/100)*2;
        return (
            <>
                <HeaderComponent>Hien Ho's Portfolio</HeaderComponent>
                <View style={styles.container}>
                    <Carousel
                        layout={'default'}
                        ref={ref => this.carousel = ref}
                        data={this.state.carouselItems}
                        //windowSize={1}
                        // change slideWidth={...} to sliderWidth={...}
                        sliderWidth={sliderWidth}
                        itemWidth={itemWidth}
                        renderItem={this._renderItem}
                        contentContainerCustomStyle={{alignItems: 'center'}}
                        onSnapToItem={index => this.setState({activeIndex: index})}
                        inactiveSlideOpacity={0}
                        loop={true}
                        autoplay={true}
                        autoplayDelay={500}
                        autoplayInterval={3000}
                    />
                    <Pagination
                        dotsLength={this.state.carouselItems.length}
                        activeDotIndex={this.state.activeIndex}
                    />
                    <View>
                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.navigation.navigate('WelcomePage')} >
                            <Text style={styles.buttonText}>Learn More About Me</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 10
    },
    button: {
        backgroundColor: 'black',
        height: 48,
        marginTop: 44,
        marginBottom: 20,
        marginLeft: 50,
        marginRight: 50,
        borderRadius: 30,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 16,
        fontWeight: 'bold'
    },
    slide: {
        backgroundColor:'floralwhite',
        borderRadius: 5,
        height: 300
    },
    image: {
        height: 300,
        width: '100%'
    }
})