import React, { Component } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MainComponent from './views/MainComponent';
import { StyleSheet, TouchableOpacity } from 'react-native';
import WelcomePage from './views/WelcomePage';
import AboutPage from './views/AboutPage';
import SkillsPage from './views/SkillsPage';
import TimelinePage from './views/TimelinePage';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faDoorOpen, faUser, faList, faHourglassHalf, faHeart } from '@fortawesome/free-solid-svg-icons'

const Stack = createStackNavigator();
const BottomTabs = createBottomTabNavigator();

export default class App extends Component {
  state = {
    isPortfolioTab: true,
    isMeTab: false,
    mainTitle: 'Portfolio'
  }

  createHomeTabs = ()  => {
    return (
      <BottomTabs.Navigator tabBarOptions={{labelPosition:'below-icon', activeTintColor:'black'}}>
          <BottomTabs.Screen
            listeners={{
              tabPress: e => {
                this.state.isPortfolioTab = true 
                this.state.isMeTab = false 
                this.state.mainTitle = 'WelcomePage'
              }
            }}
            name='Welcome'
            component={WelcomePage}
            options={{tabBarIcon: ({ color, size }) => (<FontAwesomeIcon icon={ faDoorOpen } color={color} size={size} />)}}
          ></BottomTabs.Screen>
          <BottomTabs.Screen
            listeners={{
              tabPress: e => {
                this.state.isPortfolioTab = false 
                this.state.isMeTab = false 
                this.state.mainTitle = 'AboutPage'
              },
            }}
            name='About'
            component={AboutPage}
            options={{tabBarIcon: ({ color, size }) => (<FontAwesomeIcon icon={ faUser } color={color} size={size} />)}}
          ></BottomTabs.Screen>
          <BottomTabs.Screen
            listeners={{
              tabPress: e => {
                this.state.isPortfolioTab = false,
                this.state.isMeTab = true,
                this.state.mainTitle = 'SkillsPage'
              }
            }}
            name='Skills'
            component={SkillsPage}
            options={{tabBarIcon: ({ color, size }) => (<FontAwesomeIcon icon={ faList } color={color} size={size} />)}}
          ></BottomTabs.Screen>
          <BottomTabs.Screen
            listeners={{
              tabPress: e => {
                this.state.isPortfolioTab = false,
                this.state.isMeTab = true,
                this.state.mainTitle = 'TimelinePage'
              }
            }}
            name='Timeline'
            component={TimelinePage}
            options={{tabBarIcon: ({ color, size }) => (<FontAwesomeIcon icon={ faHourglassHalf } color={color} size={size} />)}}
          ></BottomTabs.Screen>
      </BottomTabs.Navigator>
      
    )
  } 
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="MainComponent">
          <Stack.Screen name= "MainComponent" component={MainComponent} />
          <Stack.Screen name= "WelcomePage" children={this.createHomeTabs} options={
            ({ route, navigation }) => {
              let options;
              if (!route?.state?.index)
                options = { 
                  headerRight: () => (
                    <TouchableOpacity
                      style = {styles.button}
                      onPress = {() => navigation.navigate('')}
                    >
                      {/* <Ionicons name="ios-add-circle" color='black' size={30}/>  */}
                    </TouchableOpacity>
                  )
                }              
              return {
                ...options,
                ...this.mainOptions,
                title: this.state.mainTitle,
                headerLeft: () => (
                  <TouchableOpacity
                    style = {styles.button}
                    onPress = {() => navigation.navigate('MainComponent')}
                  >
                    <FontAwesomeIcon icon={ faHeart } color='black' size={30}/> 
                  </TouchableOpacity>
                ),
                gesturesEnabled: false
              }
            }
          }/>
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({
  name: {
    fontSize: 30,
    marginBottom: 30
  },
  button: {
    paddingRight: 12,
    paddingLeft: 12,
  },
  // title: {
  //   fontSize: 60,
  //   paddingBottom: 10,
  //   color: 'black',
  //   textAlign: 'right',
  //   lineHeight: 20
  // }
})
